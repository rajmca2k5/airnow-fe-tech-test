import React, { memo } from "react";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Card from "react-bootstrap/Card";

const SdkListComponent = memo(({ sdkDetails, isInstalledSDK }) => {
  return (
    <Card body className="margin-10">
      <Row>
        <Col>
          <strong>
            {isInstalledSDK ? "Installed SDK's" : "Uninstalled SDK's"}
          </strong>
        </Col>
        <Col className="align-right">
          <strong>{sdkDetails?.totalSDK}</strong>
        </Col>
      </Row>
      <Row>
        <Col>
          Latest Update : {sdkDetails?.latestUpdatedDate}
          <hr />
        </Col>
      </Row>
      <Row>
        <Col>
          <Row>
            {sdkDetails?.categories?.map((category) => (
              <Col xs={6}>
                <div className="sdk-title">{category.name}</div>
                {category.sdkList.map((sdk) => (
                  <div>
                    {sdk.sdkName} - {sdk.lastSeenDate}
                  </div>
                ))}
              </Col>
            ))}
          </Row>
        </Col>
      </Row>
    </Card>
  );
});

export default SdkListComponent;
