import React, { Component } from "react";

class ErrorComponent extends Component {
  constructor(props) {
    super(props);
    this.state = { hasError: props.hasError || false };
  }

  componentDidCatch(error, info) {
    // Display fallback UI
    this.setState({ hasError: true });
    // eslint-disable-next-line no-console
    console.log(error);
    // eslint-disable-next-line no-console
    console.log(info);
  }
  render() {
    if (this.state.hasError) {
      return (
        <div>
          <h1>Sorry, There is some technical error</h1>
          <div>
            <p>
              {" "}
              It looks like something's gone wrong in the background. Please try
              again after sometime.{" "}
            </p>
          </div>
        </div>
      );
    }
    return this.props.children;
  }
}

export default ErrorComponent;
