import { SDK_LOAD } from "../../constants/actionTypes";

const initailState = {
  sdks: [],
  isLoading: false,
  isError: false,
};

const sdkReducer = (state = initailState, action) => {
  switch (action.type) {
    case SDK_LOAD.FETCH_SDK:
      return { ...state, isLoading: true, isError: false };

    case SDK_LOAD.FETCH_SDK_SUCCESS:
      return {
        ...state,
        isLoading: false,
        sdks: action.payload,
        isError: false,
      };

    case SDK_LOAD.FETCH_SDK_FAILED:
      return { ...state, isLoading: false, isError: true };

    default:
      return state;
  }
};

export default sdkReducer;
