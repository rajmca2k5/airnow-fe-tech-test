import thunkMiddleware from "redux-thunk";
import { createStore, applyMiddleware, combineReducers } from "redux";

import sdkReducer from "./reducers/sdkReducer";

export default createStore(
  combineReducers({ sdkReducer }),
  applyMiddleware(thunkMiddleware)
);
