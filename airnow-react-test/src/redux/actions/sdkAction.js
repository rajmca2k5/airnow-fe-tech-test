import { SDK_LOAD } from "../../constants/actionTypes";
import { fetchData } from "../../api/client";
import {
  mapInstalledSdkResponse,
  mapUnInstalledSdkResponse,
} from "../../mapper/sdkResponseMapper";

export const fetchSDKStart = () => ({
  type: SDK_LOAD.FETCH_SDK,
});

export const fetchInstalledSDKSuccess = (data) => ({
  type: SDK_LOAD.FETCH_SDK_SUCCESS,
  payload: mapInstalledSdkResponse(data),
});

export const fetchInstalledSdkFail = (error) => ({
  type: SDK_LOAD.FETCH_SDK_FAILED,
  payload: error,
});

export const fetchInstalledSDKs = () => async (dispatch) => {
  dispatch(fetchSDKStart());

  try {
    const installedSDK = await fetchData("./installed.json");
    dispatch(fetchInstalledSDKSuccess(installedSDK.data));
  } catch (error) {
    dispatch(fetchInstalledSdkFail(error));
  }
};

export const fetchUnInstalledSdkSuccess = (data) => ({
  type: SDK_LOAD.FETCH_SDK_SUCCESS,
  payload: mapUnInstalledSdkResponse(data),
});

export const fetchUnInstalledSDKs = () => async (dispatch) => {
  dispatch(fetchSDKStart());

  try {
    const unInstalledSDK = await fetchData("./uninstalled.json");

    dispatch(fetchUnInstalledSdkSuccess(unInstalledSDK.data));
  } catch (error) {
    dispatch(fetchInstalledSdkFail(error));
  }
};
