import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import ProductCategoryContainer from "./containers/ProductCategoryContainer";
import ErrorComponent from "./components/ErrorComponent";

function App() {
  return (
    <div className="App">
      <div className="contianer">
        <ErrorComponent>
          <ProductCategoryContainer />
        </ErrorComponent>
      </div>
    </div>
  );
}

export default App;
