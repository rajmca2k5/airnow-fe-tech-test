export const mapInstalledSdkResponse = (response) => {
  const { installedSdks, latestUpdatedDate, __typename } = response;

  const sdks = mapSdkData(installedSdks);

  return {
    name: __typename,
    latestUpdatedDate,
    categories: sdks,
  };
};

export const mapUnInstalledSdkResponse = (response) => {
  const { uninstalledSdks, latestUpdatedDate, __typename } = response;

  const sdks = mapSdkData(uninstalledSdks);

  return {
    name: __typename,
    latestUpdatedDate,
    categories: sdks,
  };
};

const mapSdkData = (skds) => {
  return skds.map((sdk) => {
    return { ...sdk, typeName: sdk.__typename };
  });
};
