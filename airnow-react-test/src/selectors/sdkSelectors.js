import moment from "moment";

export const getFormattedSdkList = (sdkList) => {
  const { name, latestUpdatedDate, categories } = sdkList;

  return {
    name,
    latestUpdatedDate: moment(latestUpdatedDate).format("MMM DD,YYYY"),
    totalSDK: categories?.length,
    categories: getSdkResponse(categories),
  };
};

const getSdkResponse = (sdkList) => {
  const result = [];
  sdkList &&
    sdkList.forEach((sdk) => {
      const { id, name, categories, firstSeenDate, lastSeenDate, typeName } =
        sdk;
      categories.forEach((category) => {
        const existsCate = result.findIndex((cat) => cat.name === category);
        if (existsCate > 0) {
          result[existsCate] = {
            name: category,
            id,
            sdkList: [
              ...result[existsCate].sdkList,
              {
                sdkName: name,
                firstSeenDate,
                lastSeenDate: moment(lastSeenDate).fromNow(),
                typeName,
              },
            ],
          };
        } else {
          result.push({
            name: category,
            id,
            sdkList: [
              {
                sdkName: name,
                firstSeenDate,
                lastSeenDate: moment(lastSeenDate).fromNow(),
                typeName,
              },
            ],
          });
        }
      });
    });

  return result;
};
