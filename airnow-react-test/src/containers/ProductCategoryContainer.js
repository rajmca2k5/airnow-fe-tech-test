import React, { useEffect, memo, useState } from "react";
import { connect } from "react-redux";
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import {
  fetchInstalledSDKs,
  fetchUnInstalledSDKs,
} from "../redux/actions/sdkAction";
import { getFormattedSdkList } from "../selectors/sdkSelectors";
import SdkList from "../components/SdkListComponent";

const ProductCategoryContainer = memo(
  ({ fetchInstalledSDKs, fetchUnInstalledSDKs, sdkDetails, isLoading }) => {
    const [isInstalledSDK, setInstalledSDK] = useState(true);
    useEffect(() => {
      fetchInstalledSDKs();
    }, [fetchInstalledSDKs]);

    const handleLoadInstalledSdkClick = () => {
      setInstalledSDK(true);
      fetchInstalledSDKs();
    };
    const handleLoadUnInstalledSdkClick = () => {
      setInstalledSDK(false);
      fetchUnInstalledSDKs();
    };

    return (
      <>
        <Container>
          <Row>
            <Col>
              <Card body>
                <Row>
                  <Col>
                    <Button
                      variant={isInstalledSDK ? "primary" : "outline-primary"}
                      onClick={() => handleLoadInstalledSdkClick()}
                    >
                      Installed
                    </Button>
                    <Button
                      variant={!isInstalledSDK ? "primary" : "outline-primary"}
                      onClick={() => handleLoadUnInstalledSdkClick()}
                    >
                      Uninstalled
                    </Button>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    {isLoading ? (
                      <div>Please wait......</div>
                    ) : (
                      <SdkList
                        sdkDetails={sdkDetails}
                        isInstalledSDK={isInstalledSDK}
                      />
                    )}
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
        </Container>
      </>
    );
  }
);

const mapDispatchToProps = {
  fetchInstalledSDKs,
  fetchUnInstalledSDKs,
};

const mapStateToProps = ({ sdkReducer }) => ({
  sdkDetails: getFormattedSdkList(sdkReducer.sdks),
  isLoading: sdkReducer.isLoading,
  isErrors: sdkReducer.isErrors,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductCategoryContainer);
